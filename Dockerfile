FROM ubuntu

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD app /app
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install npm

RUN npm install

CMD ["npm", "start"]
